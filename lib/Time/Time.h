#ifndef TIMING_H
#define TIMING_H

#include <Arduino.h>

#define TIME_UPDATE_INTERVAL 1000

unsigned int operator""_sec(unsigned long long);
double operator""_sec(long double);
unsigned int operator""_min(unsigned long long);
double operator""_min(long double);

using time_ms = uint32_t;

void setup_timing();
void update_time();

bool wait(time_ms&, time_ms const);

#endif
