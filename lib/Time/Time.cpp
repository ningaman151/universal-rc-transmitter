#include <Pins.h>
#include "Time.h"

unsigned int operator""_sec(unsigned long long seconds)
{
  return seconds * 1000; //seconds to milliseconds
}

double operator""_sec(long double seconds)
{
  return seconds * 1000.0;
}

unsigned int operator""_min(unsigned long long mins)
{
  return mins * 60_sec; 
}

double operator""_min(long double mins)
{
  return mins * 60.0_sec;
}

void setup_timing()
{
//  NextionDisplay::date_display.setText(rtc.getDateStr());
}

bool wait(time_ms& prev_time, time_ms const interval) 
{
  time_ms now = millis();
  if(now - prev_time >= interval) {
    prev_time = now;
    return true;
  }
  return false;
} 

//void update_time()
//{
//  static time_ms prev_time = 0;
//  
//  if(NextionDisplay::page == 0 and wait(prev_time, TIME_UPDATE_INTERVAL))
//  {
//    NextionDisplay::time_display.setText(rtc.getTimeStr());
//  }
//}
