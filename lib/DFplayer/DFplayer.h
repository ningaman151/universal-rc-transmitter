#ifndef DF_PLAYER_H
#define DF_PLAYER_H

#define DF_SERIAL Serial2
#define BUSY_PIN 9

#define DF_STARTUP_VOL 20 //0 to 30

# define Start_Byte 0x7E
# define Version_Byte 0xFF
# define Command_Length 0x06
# define End_Byte 0xEF
# define Acknowledge 0x00 //Returns info with command 0x41 [0x01: info, 0x00: no info]

#define c_play_file  0x0F
#define c_set_volume 0x06
#define c_stop_track 0x16
#define c_play       0x0D
#define c_pause      0x0E
#define c_setvolume  0x06
#define c_initialize 0x3F
#define c_track_mode 0x08
#define c_stop       0x16
#define c_equalizer  0x07
#define C_RESET_MODULE 0x0C
#define C_REPEAT_PLAY 0x11
#define C_QUERY_VOLUME 0x43

void setup_DF();

void DF_initialize();
void DF_playfile(byte folder_num, byte file_num);
void DF_track_mode(byte track_mode);
void DF_pause();
void DF_stop();
void DF_play();
void DF_setVolume(int vol);
void DF_reset();
void DF_repeat_play(byte mode);
void DF_query_status();
byte DF_query_volume();

void DF_execute_CMD(byte CMD, byte Par1, byte Par2);

#endif
