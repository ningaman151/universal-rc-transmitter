#include <Arduino.h>
#include "DFplayer.h"
#include <Pins.h>

void setup_DF()
{
  DF_SERIAL.begin(9600);
  DF_initialize();

  // while(digitalRead(DF_BUSY_PIN))
  // {
  //   DF_setVolume(DF_STARTUP_VOL);
  //   DF_playfile(1, 1);
  // }

  DF_setVolume(DF_STARTUP_VOL);
  delay(50);
  DF_playfile(1, 1);

  //  delay(2000);

  //  DF_playfile(1, 5);


}

void DF_initialize()
{
  DF_execute_CMD(c_initialize, 0, 0);
  delay(50);
}

void DF_playfile(byte folder_num, byte file_num)
{
  DF_execute_CMD(c_play_file, folder_num, file_num);
  //delay(30);
}

void DF_track_mode(byte track_mode)
{
  DF_execute_CMD(c_track_mode, 0, track_mode);
}

void DF_reset()
{
   DF_execute_CMD(C_RESET_MODULE, 0, 0);
}

void DF_repeat_play(byte mode)
{
  DF_execute_CMD(C_REPEAT_PLAY, 0, 0);
}


//void DF_pause()
//{
//  DF_execute_CMD(c_pause,0,0);
//  //delay(30);
//}
//
//void DF_stop()
//{
//  DF_execute_CMD(c_stop,0,0);
//}
//
//void DF_play()
//{
//  DF_execute_CMD(c_play,0,1); 
//  //delay(30);
//}

void DF_setVolume(int vol)
{
  DF_execute_CMD(c_setvolume, 0, vol); // Set the volume (0x00~0x30)
  //delay(30);
}

byte DF_query_volume()
{
  // DF_execute_CMD(C_QUERY_VOLUME, 0, 0);

  // while(!(DF_SERIAL.available() == 10))
  // { 

  // }

  // Serial.println(DF_SERIAL.read());
  // Serial.println(DF_SERIAL.read());

  return 0; //return volume value
}

//void DF_query_status()
//{
//  DF_execute_CMD(C_QUERY_STATUS, 0, 0);
//}

void DF_execute_CMD(byte CMD, byte Par1, byte Par2)
  // Excecute the command and parameters
{
  // Calculate the checksum (2 bytes)
  word checksum = -(Version_Byte + Command_Length + CMD + Acknowledge + Par1 + Par2);
  // Build the command line
  byte Command_line[10] = { Start_Byte, Version_Byte, Command_Length, CMD, Acknowledge,
  Par1, Par2, highByte(checksum), lowByte(checksum), End_Byte};
  //Send the command line to the module
  for (byte k=0; k<10; k++)
  {
    DF_SERIAL.write(Command_line[k]);
  }
}
