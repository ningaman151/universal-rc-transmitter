#ifndef PINS_H
#define PINS_H

#include <Arduino.h>

//#define PIN 1
#define LSX_PIN A3 //ch1
#define LSY_PIN A2 //ch2
#define RSX_PIN A0 //ch3
#define RSY_PIN A1 //ch4
#define SLIDER_PIN A4 //ch5
#define POTENTIOMETER_PIN A5 //ch6
#define LEFT_CALIPER 43 //ch7
#define RIGHT_CALIPER 42 //ch8
#define LEFT_BUTTON 23 //ch9
#define RIGHT_BUTTON 22 //ch10
#define RED_BUTTON 24 //ch11
#define GREEN_SWITCH 25 //ch12
 

// #define SET_BTN_PIN 5
// #define VOL_BTN_PIN 6
// #define SW1_PIN 7
// #define LATCH_BTN_PIN 8
// #define SW2_UP 11
// #define SW2_DOWN 10 // change this

#define SD_CS_PIN 53
#define RADIO_CE 7
#define RADIO_CSN 8

#define DF_BUSY_PIN 9
#define BATTERY_PIN A6

#define NEX_SERIAL Serial1 // Pins 18 and 19
#define DF_SERIAL Serial2  // Pins 16 and 17

// SPI pins 50 51 52 53

#endif
