#include <Arduino.h>
#include "Mathematics.h"

float exp_mult(const byte pin, const float ch_exp)
{
  float exp_val;
  int pin_read = analogRead(pin);

  if (pin_read - 512 > 0)
  {
    float m = -ch_exp / 512.0F;
    float b = -m * 1023.0F;
    exp_val = m * pin_read + b;   //128
  }
  else
  {
    float m = ch_exp / 512.0F;
    exp_val = m * pin_read;
  }

  if (exp_val <= ch_exp)
    return 0.0F;
  else
    return exp(exp_val);
}
