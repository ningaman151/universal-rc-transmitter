#ifndef SD_CARD_H
#define SD_CARD_H

void sd_setup();

bool sd_save_trims();
bool sd_save_expos();
bool sd_save_duals();
bool sd_save_inverse();

bool sd_read_trims();
bool sd_read_expos();
bool sd_read_duals();
bool sd_read_inverse();

#endif
