#include <SD.h>
#include <Pins.h>
#include "SD_card.h"
#include <Settings.h>

void sd_setup()
{
  if(!SD.begin(SD_CS_PIN))
  {
    //handle failure
  }
  
  setup_settings();
}

bool sd_save_trims()
{
  SD.remove("trim.txt");
  File sd = SD.open("trim.txt", FILE_WRITE);

  if(sd)
  {
    sd.write((byte*)&settings.trim, sizeof(settings.trim));
    sd.close();

    return true;
  }

  return false;
}

bool sd_save_expos()
{
  SD.remove("expo.txt");
  File sd = SD.open("expo.txt", FILE_WRITE);

  if(sd)
  {
    sd.write((byte*)&settings.expo, sizeof(settings.expo));
    sd.close();

    return true;
  }

  return false;
}

bool sd_save_duals()
{
  SD.remove("dual.txt");
  File sd = SD.open("dual.txt", FILE_WRITE);

  if(sd)
  {
    sd.write((byte*)&settings.dualrates, sizeof(settings.dualrates));
    sd.close();

    return true;
  }

  return false;
}

bool sd_save_inverse()
{
  SD.remove("inverse.txt");
  File sd = SD.open("inverse.txt", FILE_WRITE);

  if(sd)
  {
    sd.write((byte*)&settings.inverse, sizeof(settings.inverse));
    sd.close();

    return true;
  }

  return false;
}

bool sd_read_trims()
{
  File sd = SD.open("trim.txt");

  if(sd)
  {
    sd.read((byte*)&settings.trim, sizeof(settings.trim));
    sd.close();

    return true;
  }

  return false;
}


bool sd_read_duals()
{
  File sd = SD.open("dual.txt", FILE_READ);
  
  if(sd)
  {
    sd.read((byte*)&settings.dualrates, sizeof(settings.dualrates)); 
    sd.close();

    return true;
  }

  return false;
}

bool sd_read_expos()
{
  File sd = SD.open("expo.txt", FILE_READ);

  if(sd)
  {
    sd.read((byte*)&settings.expo, sizeof(settings.expo));
    sd.close();

    return true;
  }

  return false;
}

bool sd_read_inverse()
{
  File sd = SD.open("inverse.txt", FILE_READ);
  
  if(sd)
  {
    sd.read((byte*)&settings.inverse, sizeof(settings.inverse)); 
    sd.close();

    return true;
  }

  return false;
}
