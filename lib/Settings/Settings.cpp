#include "Settings.h"
#include <SD_card.h>

Settings settings;

void setup_settings()
{
  sd_read_trims();
  sd_read_duals();
  sd_read_expos();
  sd_read_inverse();
}
