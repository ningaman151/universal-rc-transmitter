#ifndef SETTINGS_H
#define SETTINGS_H

#include <Arduino.h>

#define TRIM_INCR 2
#define EXPO_INCR 0.1F
#define DUAL_INCR 1

struct Settings
{
  struct Trimmings
  {
    int ch1 = 0;
    int ch2 = 0;
    int ch3 = 0;
    int ch4 = 0;
  } trim;

  struct DualRates
  {
    bool dual_mode = false;
    
    byte ch1 = 100;
    byte ch2 = 100;
    byte ch3 = 100;
    byte ch4 = 100;
  } dualrates;

  struct Expos
  {
    bool expo_mode = false; //mode (normal vs exp)

    float ch1 = -1.0F;
    float ch2 = -1.0F;
    float ch3 = -1.0F;
    float ch4 = -1.0F;

    /*
    float m_up1, m_low1;
    float m_up2, m_low2;
    float m_up3, m_low3;
    float m_up4, m_low4;
    */
  } expo;

  struct Inverse
  {
    bool ch1 = false;
    bool ch2 = false;
    bool ch3 = false;
    bool ch4 = false;
    //Add more inverse options for channels
  } inverse;
};

extern Settings settings;

void setup_settings();

#endif
