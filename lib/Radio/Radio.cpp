#include "Radio.h"
#include <Pins.h>

RF24 transmitter(RADIO_CE,  RADIO_CSN);
ChannelData tx_data;

uint8_t address[][6] = {"RCSYS"};

bool radio_setup()
{
  //while(!transmitter.begin());

  if(!transmitter.begin())
  {
    //Handle failure
    //while(true);

    return false;
  }

  transmitter.setAutoAck(false);
  transmitter.setRetries(0, 0);
  transmitter.setPALevel(RF24_PA_MAX);
  transmitter.setDataRate(RF24_250KBPS);
  transmitter.setPayloadSize(sizeof(tx_data));
  transmitter.stopListening();
  transmitter.openWritingPipe(address[0]);

  return true;
}
