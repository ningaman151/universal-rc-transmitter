#ifndef RADIO_H
#define RADIO_H

#include <RF24.h>
#include <Arduino.h>
#include <Data.h>

extern RF24 transmitter;

extern ChannelData tx_data;

bool radio_setup();

#endif
