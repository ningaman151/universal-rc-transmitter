#include <Arduino.h>

#include <SD.h>
#include <Settings.h>
#include "Nextion.h"
#include <SD_card.h>
#include "Components.h"
#include <Battery.h>
#include <Radio.h>
#include <DFplayer.h>

//#define DEBUG

void setup()
{
  setup_DF();
  sd_setup();
  radio_setup();
  battery_setup();
  nex_setup();

  //  reset_input();
}

void loop()
{
  nex_loop();
}