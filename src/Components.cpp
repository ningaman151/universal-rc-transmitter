#include <Arduino.h>
#include "Components.h"
#include <Settings.h>
#include <Radio.h>
#include <Pins.h>
#include <Mathematics.h>

namespace
{
byte ch1_dual_mult;
byte ch2_dual_mult;
byte ch3_dual_mult;
byte ch4_dual_mult;

float ch1_exp_mult;
float ch2_exp_mult;
float ch3_exp_mult;
float ch4_exp_mult;
}

void handle_joysticks()
{
  if (settings.dualrates.dual_mode)
  {
    ch1_dual_mult = settings.dualrates.ch1;
    ch2_dual_mult = settings.dualrates.ch2;
    ch3_dual_mult = settings.dualrates.ch3;
    ch4_dual_mult = settings.dualrates.ch4;
  }
  else
  {
    ch1_dual_mult = 100.0F;
    ch2_dual_mult = 100.0F;
    ch3_dual_mult = 100.0F;
    ch4_dual_mult = 100.0F;
  }

  if (settings.expo.expo_mode)
  {
    ch1_exp_mult = exp_mult(LSX_PIN, settings.expo.ch1);
    ch2_exp_mult = exp_mult(LSY_PIN, settings.expo.ch2);
    ch3_exp_mult = exp_mult(RSX_PIN, settings.expo.ch3); 
    ch4_exp_mult = exp_mult(RSY_PIN, settings.expo.ch4);
  }
  else
  {
    ch1_exp_mult = 1;
    ch2_exp_mult = 1;
    ch3_exp_mult = 1; 
    ch4_exp_mult = 1;
  }

//  if (!settings.expo.expo_mode)
//  {
//    //normal mode
//    joysticks_normal();
//  }
//  else
//  {
//    joysticks_expo();
//  }

  //tx_data.ch1 = map(analogRead(LSX_PIN), 0, 1023, 1000)

  tx_data.ch1 = map(512 + (abs(1023 * settings.inverse.ch1 - analogRead(LSX_PIN)) - 512) * (ch1_dual_mult / 100.0F) * ch1_exp_mult + settings.trim.ch1, 0, 1023, 1000, 2000);
  tx_data.ch2 = map(512 + (abs(1023 * settings.inverse.ch2 - analogRead(LSY_PIN)) - 512) * (ch2_dual_mult / 100.0F) * ch2_exp_mult + settings.trim.ch2, 0, 1023, 1000, 2000);
  tx_data.ch3 = map(512 + (abs(1023 * settings.inverse.ch3 - analogRead(RSX_PIN)) - 512) * (ch3_dual_mult / 100.0F) * ch3_exp_mult + settings.trim.ch3, 0, 1023, 1000, 2000);
  tx_data.ch4 = map(512 + (abs(1023 * settings.inverse.ch4 - analogRead(RSY_PIN)) - 512) * (ch4_dual_mult / 100.0F) * ch4_exp_mult + settings.trim.ch4, 0, 1023, 1000, 2000);

  tx_data.ch1 = constrain(tx_data.ch1, 1000, 2000);
  tx_data.ch2 = constrain(tx_data.ch2, 1000, 2000);
  tx_data.ch3 = constrain(tx_data.ch3, 1000, 2000);
  tx_data.ch4 = constrain(tx_data.ch4, 1000, 2000);
}

//void joysticks_normal()
//{
//  data.ch1 = 512 + (analogRead(LSX_PIN) - 512) * (ch1_dual_mult / 100.0F) + settings.trim.ch1;
//  data.ch2 = 512 + (analogRead(LSY_PIN) - 512) * (ch2_dual_mult / 100.0F) + settings.trim.ch2;
//  data.ch3 = 512 + (analogRead(RSX_PIN) - 512) * (ch3_dual_mult / 100.0F) + settings.trim.ch3;
//  data.ch4 = 512 + (analogRead(RSY_PIN) - 512) * (ch4_dual_mult / 100.0F) + settings.trim.ch4;
//}
//
//void joysticks_expo()
//{
//  data.ch1 = 512 + (analogRead(LSX_PIN) - 512) * (ch1_dual_mult / 100.0F) * exp_mult(LSX_PIN, settings.expo.ch1) + settings.trim.ch1;
//  data.ch2 = 512 + (analogRead(LSY_PIN) - 512) * (ch2_dual_mult / 100.0F) * exp_mult(LSY_PIN, settings.expo.ch2) + settings.trim.ch2;
//  data.ch3 = 512 + (analogRead(RSX_PIN) - 512) * (ch3_dual_mult / 100.0F) * exp_mult(RSX_PIN, settings.expo.ch3) + settings.trim.ch3;
//  data.ch4 = 512 + (analogRead(RSY_PIN) - 512) * (ch4_dual_mult / 100.0F) * exp_mult(RSY_PIN, settings.expo.ch4) + settings.trim.ch4;
//}

void handle_right_panel()
{
  tx_data.ch5 = map(analogRead(SLIDER_PIN), 0, 1023, 1000, 2000);
  tx_data.ch6 = map(analogRead(POTENTIOMETER_PIN), 0, 1023, 1000, 2000);
  tx_data.ch7 = 1000 + digitalRead(LEFT_CALIPER) * 1000;
  tx_data.ch8 = 1000 + digitalRead(RIGHT_CALIPER) * 1000;
  tx_data.ch9 = 2000 - digitalRead(LEFT_BUTTON) * 1000;
  tx_data.ch10 = 2000 - digitalRead(RIGHT_BUTTON) * 1000;
  // tx_data.ch9 = 1000 + !digitalRead(LEFT_BUTTON) * 1000;
  // tx_data.ch10 = 1000 + !digitalRead(RIGHT_BUTTON) * 1000;

  tx_data.ch11 = 1000 + digitalRead(RED_BUTTON) * 1000;
  tx_data.ch12 = 1000 + digitalRead(GREEN_SWITCH) * 1000;
}

int handle_on_off_on(byte pin_up, byte pin_down)
{
  if (!digitalRead(pin_up))
    return OFO_SW_UP;

  if (!digitalRead(pin_down))
    return OFO_SW_DOWN;

  return OFO_SW_MIDDLE;
} 

void reset_input()
{
  tx_data.ch1 = map(512 + settings.trim.ch1, 0, 1023, 1000, 2000);
  tx_data.ch2 = map(512 + settings.trim.ch2, 0, 1023, 1000, 2000);
  tx_data.ch3 = map(512 + settings.trim.ch3, 0, 1023, 1000, 2000);
  tx_data.ch4 = map(512 + settings.trim.ch4, 0, 1023, 1000, 2000);

  tx_data.ch1 = constrain(tx_data.ch1, 1000, 2000);
  tx_data.ch2 = constrain(tx_data.ch2, 1000, 2000);
  tx_data.ch3 = constrain(tx_data.ch3, 1000, 2000);
  tx_data.ch4 = constrain(tx_data.ch4, 1000, 2000);
}
