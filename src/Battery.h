#ifndef BATTERY_H
#define BATTERY_H

#include <CircularBuffer.h>

#define BATTERY_MAX_VOLTAGE 4.85F //Find appropriate value

#define BUFF_SIZE 6
#define BUFF_INTERVAL 1250

void battery_setup();
void battery_level(const char*);

#endif
