#ifndef NEXTION_H
#define NEXTION_H

#include <Pins.h>

//#ifdef NEX_SERIAL_NUM
//#if NEX_SERIAL_NUM == SERIAL0
//  #define NEX_SERIAL Serial
//#elif NEX_SERIAL_NUM == SERIAL1
//  #define NEX_SERIAL Serial1
//#elif NEX_SERIAL_NUM == SERIAL2
//  #define NEX_SERIAL Serial2
//#elif NEX_SERIAL_NUM == SERIAL3
//  #define NEX_SERIAL Serial3
//#endif
//#endif

#define BATTERY_NUM "n1"
#define BATTERY_BAR "j0"
//#define BAT_BLINK_TIMER "tm0"

#define TRIMS_CH1 "n0"
#define TRIMS_CH2 "n1"
#define TRIMS_CH3 "n2"
#define TRIMS_CH4 "n3"
#define TRIMS_VAL_CH1 "n4"
#define TRIMS_VAL_CH2 "n5"
#define TRIMS_VAL_CH3 "n6"
#define TRIMS_VAL_CH4 "n7"
#define TRIMS_BAR_CH1 "j0"
#define TRIMS_BAR_CH2 "j1"
#define TRIMS_BAR_CH3 "j2"
#define TRIMS_BAR_CH4 "j3"


#define EXPO_MODE_BTN "bt0"
#define EXPO_CH1 "x0"
#define EXPO_CH2 "x1"
#define EXPO_CH3 "x2"
#define EXPO_CH4 "x3"
#define EXPO_VAL_CH1 "n4"
#define EXPO_VAL_CH2 "n5"
#define EXPO_VAL_CH3 "n6"
#define EXPO_VAL_CH4 "n7"
#define EXPO_BAR_CH1 "j0"
#define EXPO_BAR_CH2 "j1"
#define EXPO_BAR_CH3 "j2"
#define EXPO_BAR_CH4 "j3"

#define DUAL_MODE_BTN "bt0"
#define DUAL_CH1 "n0"
#define DUAL_CH2 "n1"
#define DUAL_CH3 "n2"
#define DUAL_CH4 "n3"
#define DUAL_VAL_CH1 "n4"
#define DUAL_VAL_CH2 "n5"
#define DUAL_VAL_CH3 "n6"
#define DUAL_VAL_CH4 "n7"
#define DUAL_BAR_CH1 "j0"
#define DUAL_BAR_CH2 "j1"
#define DUAL_BAR_CH3 "j2"
#define DUAL_BAR_CH4 "j3"

#define JS_CH1_VAL "n0"
#define JS_CH2_VAL "n1"
#define JS_CH3_VAL "n2"
#define JS_CH4_VAL "n3"
#define JS_CH1_BAR "j0"
#define JS_CH2_BAR "j1"
#define JS_CH3_BAR "j2"
#define JS_CH4_BAR "j3"

#define INVERSE_CH1_BTN "bt0"
#define INVERSE_CH2_BTN "bt1"
#define INVERSE_CH3_BTN "bt2"
#define INVERSE_CH4_BTN "bt3"
#define INVERSE_CH1_VAL "n0"
#define INVERSE_CH2_VAL "n1"
#define INVERSE_CH3_VAL "n2"
#define INVERSE_CH4_VAL "n3"
#define INVERSE_CH1_BAR "j0"
#define INVERSE_CH2_BAR "j1"
#define INVERSE_CH3_BAR "j2"
#define INVERSE_CH4_BAR "j3"

#define BLACK 0
#define BROWN 48192
#define GREEN 2016
#define YELLOW 65504
#define RED 63488
#define GRAY 33840
#define WHITE 65535

namespace Pages
{
  const byte splash = 0;
  const byte home = 1;
  const byte settings = 2;
  const byte trimming = 3;
  const byte expo = 4;
  const byte dualrates = 5;
  const byte joysticks = 6;
  const byte inverse = 7;
}

struct DisplayData
{
  byte page = 1;
};

extern DisplayData nexdata;

namespace Nex
{
  void terminate_packet();
  void setColorPco(char const *, unsigned);
  void setColorBco(char const*, unsigned);
  void setText(char const *, char const *);
  void setValue(char const *, long);
  void setEnable(char const*, bool);
  void changePage(byte);
}

void nex_setup();
void handle_nextion_data_rx();
void nex_loop();

#endif
