#ifndef COMPONENTS_H
#define COMPONENTS_H

#define OFO_SW_UP 2000
#define OFO_SW_DOWN 1000
#define OFO_SW_MIDDLE 1500

void handle_joysticks();
void joysticks_normal();
void joysticks_expo();

void reset_input();

void handle_right_panel();
int handle_on_off_on(byte, byte);

#endif
