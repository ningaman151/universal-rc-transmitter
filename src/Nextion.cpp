#include <Arduino.h>
#include "Nextion.h"
#include "Components.h"
#include <Radio.h>
#include <Pins.h>
#include <Settings.h>
#include <SD_card.h>
#include <Battery.h>

DisplayData nexdata;

namespace
{
  namespace UI
  {
    namespace Type
    {
      const byte page = 1;
      const byte button = 2;
      const byte text = 3;
      const byte dualstatebutton = 4;
    }

    namespace Pages
    {
      namespace Action
      {
        const byte pre_init = 1;
      }
    }

    namespace Buttons
    {
      namespace Action
      {
        const byte press = 1;
        const byte pop = 2;
      }

      namespace P_home
      {
        const byte settings = 1;
      }
      namespace P_settings
      {
        const byte trim = 1;
      }
      namespace P_trim
      {
        const byte back = 1;
        const byte ch1_trim_plus = 2;
        const byte ch1_trim_minus = 3;
        const byte ch2_trim_plus = 4;
        const byte ch2_trim_minus = 5;
        const byte ch3_trim_plus = 6;
        const byte ch3_trim_minus = 7;
        const byte ch4_trim_plus = 8;
        const byte ch4_trim_minus = 9;
        const byte reset_trims = 10;
      }
      namespace P_expo
      {
        const byte back = 1;
        const byte ch1_expo_plus = 2;
        const byte ch1_expo_minus = 3;
        const byte ch2_expo_plus = 4;
        const byte ch2_expo_minus = 5;
        const byte ch3_expo_plus = 6;
        const byte ch3_expo_minus = 7;
        const byte ch4_expo_plus = 8;
        const byte ch4_expo_minus = 9;
        const byte reset_expos = 10;
      }
      namespace P_dualrates
      {
        const byte back = 1;
        const byte ch1_dual_plus = 2;
        const byte ch1_dual_minus = 3;
        const byte ch2_dual_plus = 4;
        const byte ch2_dual_minus = 5;
        const byte ch3_dual_plus = 6;
        const byte ch3_dual_minus = 7;
        const byte ch4_dual_plus = 8;
        const byte ch4_dual_minus = 9;
        const byte reset_duals = 10;
      }
    }
    namespace DualStateButton
    {
      namespace Action
      {
        const byte pop = 1;
      }
      namespace P_expo
      {
        const byte expo_mode = 1;
      }
      namespace P_dualrates
      {
        const byte dual_mode = 1;
      }
      namespace P_inverse
      {
        const byte inv_ch1 = 1;
        const byte inv_ch2 = 2;
        const byte inv_ch3 = 3;
        const byte inv_ch4 = 4;
      }
    }
  }
}

void nex_setup()
{
  NEX_SERIAL.begin(230400);

  delay(1000);
  Nex::changePage(Pages::home);
}

void nex_loop()
{
  handle_nextion_data_rx();

  switch (nexdata.page)
  {
  case Pages::home:
    battery_level(BATTERY_NUM);

    handle_right_panel();
    handle_joysticks();

    transmitter.write(&tx_data, sizeof(tx_data));
    break;

  case Pages::settings:
    reset_input();
    handle_right_panel();

    transmitter.write(&tx_data, sizeof(tx_data));
    break;    
  
  case Pages::trimming:
    handle_joysticks();
    Nex::setValue(TRIMS_VAL_CH1, tx_data.ch1);
    Nex::setValue(TRIMS_VAL_CH2, tx_data.ch2);
    Nex::setValue(TRIMS_VAL_CH3, tx_data.ch3);
    Nex::setValue(TRIMS_VAL_CH4, tx_data.ch4);

    Nex::setValue(TRIMS_BAR_CH1, map(tx_data.ch1, 1000, 2000, 0, 100));
    Nex::setValue(TRIMS_BAR_CH2, map(tx_data.ch2, 1000, 2000, 0, 100));
    Nex::setValue(TRIMS_BAR_CH3, map(tx_data.ch3, 1000, 2000, 0, 100));
    Nex::setValue(TRIMS_BAR_CH4, map(tx_data.ch4, 1000, 2000, 0, 100));

    reset_input();
    handle_right_panel();

    transmitter.write(&tx_data, sizeof(tx_data));
    break;

  case Pages::expo:
    handle_joysticks();
    Nex::setValue(EXPO_VAL_CH1, tx_data.ch1);
    Nex::setValue(EXPO_VAL_CH2, tx_data.ch2);
    Nex::setValue(EXPO_VAL_CH3, tx_data.ch3);
    Nex::setValue(EXPO_VAL_CH4, tx_data.ch4);

    Nex::setValue(EXPO_BAR_CH1, map(tx_data.ch1, 1000, 2000, 0, 100));
    Nex::setValue(EXPO_BAR_CH2, map(tx_data.ch2, 1000, 2000, 0, 100));
    Nex::setValue(EXPO_BAR_CH3, map(tx_data.ch3, 1000, 2000, 0, 100));
    Nex::setValue(EXPO_BAR_CH4, map(tx_data.ch4, 1000, 2000, 0, 100));

    reset_input();
    handle_right_panel();

    transmitter.write(&tx_data, sizeof(tx_data));
    break;

  case Pages::dualrates:
    handle_joysticks();
    Nex::setValue(DUAL_VAL_CH1, tx_data.ch1);
    Nex::setValue(DUAL_VAL_CH2, tx_data.ch2);
    Nex::setValue(DUAL_VAL_CH3, tx_data.ch3);
    Nex::setValue(DUAL_VAL_CH4, tx_data.ch4);

    Nex::setValue(DUAL_BAR_CH1, map(tx_data.ch1, 1000, 2000, 0, 100));
    Nex::setValue(DUAL_BAR_CH2, map(tx_data.ch2, 1000, 2000, 0, 100));
    Nex::setValue(DUAL_BAR_CH3, map(tx_data.ch3, 1000, 2000, 0, 100));
    Nex::setValue(DUAL_BAR_CH4, map(tx_data.ch4, 1000, 2000, 0, 100));

    reset_input();
    handle_right_panel();

    transmitter.write(&tx_data, sizeof(tx_data));
    break;

  case Pages::joysticks:
    handle_joysticks();
    Nex::setValue(JS_CH1_VAL, tx_data.ch1);
    Nex::setValue(JS_CH2_VAL, tx_data.ch2);
    Nex::setValue(JS_CH3_VAL, tx_data.ch3);
    Nex::setValue(JS_CH4_VAL, tx_data.ch4);

    Nex::setValue(JS_CH1_BAR, map(tx_data.ch1, 1000, 2000, 0, 100));
    Nex::setValue(JS_CH2_BAR, map(tx_data.ch2, 1000, 2000, 0, 100));
    Nex::setValue(JS_CH3_BAR, map(tx_data.ch3, 1000, 2000, 0, 100));
    Nex::setValue(JS_CH4_BAR, map(tx_data.ch4, 1000, 2000, 0, 100));

    reset_input();
    handle_right_panel();

    transmitter.write(&tx_data, sizeof(tx_data));
    break;

  case Pages::inverse:
    handle_joysticks();

    Nex::setValue(INVERSE_CH1_VAL, tx_data.ch1);
    Nex::setValue(INVERSE_CH2_VAL, tx_data.ch2);
    Nex::setValue(INVERSE_CH3_VAL, tx_data.ch3);
    Nex::setValue(INVERSE_CH4_VAL, tx_data.ch4);

    Nex::setValue(INVERSE_CH1_BAR, map(tx_data.ch1, 1000, 2000, 0, 100));
    Nex::setValue(INVERSE_CH2_BAR, map(tx_data.ch2, 1000, 2000, 0, 100));
    Nex::setValue(INVERSE_CH3_BAR, map(tx_data.ch3, 1000, 2000, 0, 100));
    Nex::setValue(INVERSE_CH4_BAR, map(tx_data.ch4, 1000, 2000, 0, 100));

    reset_input();
    handle_right_panel();

    transmitter.write(&tx_data, sizeof(tx_data));
    break;
  }
}

void handle_nextion_data_rx()
{
  if (NEX_SERIAL.available() > 0 and NEX_SERIAL.available() % 4 == 0)
  {
    byte page = NEX_SERIAL.read();
    byte type = NEX_SERIAL.read();
    byte index = NEX_SERIAL.read();
    byte command = NEX_SERIAL.read();

    switch (page)
    {
      //      case Pages::splash:
      //        switch(type)
      //        {
      //          case UI::Type::page:
      //            switch(command)
      //            {
      //              case UI::Pages::Action::pre_init:
      //                Nex::changePage(Pages::home);
      //                break;
      //            }
      //            break;
      //        }
      //        break;

    case Pages::home:
      switch (type)
      {
      case UI::Type::page:
        switch (command)
        {
        case UI::Pages::Action::pre_init:
          nexdata.page = Pages::home;
          break;
        }
        break;

        //          case UI::Type::button:
        //            switch (index)
        //            {
        //              case UI::Buttons::P_home::settings:
        //                switch (command)
        //                {
        //                  case UI::Buttons::Action::press:
        //                    Nex::setText("t0", "Hello world");
        //                    break;
        //
        //                  case UI::Buttons::Action::pop:
        //                    //send to different Serial (Serial is used for serial monitor)
        //                    NEX_SERIAL.print("Button pressed");
        //                    //execute function here
        //                    break;
        //                }
        //                break;
        //            }
        //            break;
        //
        //          case UI::Type::text:
        //            //home_page_text(undex_num, command_num);
        //            break;
      }
      break;

    case Pages::settings:
      switch (type)
      {
      case UI::Type::page:
        switch (command)
        {
        case UI::Pages::Action::pre_init:
          nexdata.page = Pages::settings;
          break;
        }
      }
      break;

    case Pages::trimming:
      switch (type)
      {
      case UI::Type::page:
        switch (command)
        {
        case UI::Pages::Action::pre_init:
          Nex::setValue(TRIMS_CH1, settings.trim.ch1);
          Nex::setValue(TRIMS_CH2, settings.trim.ch2);
          Nex::setValue(TRIMS_CH3, settings.trim.ch3);
          Nex::setValue(TRIMS_CH4, settings.trim.ch4);

          nexdata.page = Pages::trimming;
          break;
        }
        break;

      case UI::Type::button:
        switch (index)
        {
        case UI::Buttons::P_trim::reset_trims:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.trim.ch1 = 0;
            settings.trim.ch2 = 0;
            settings.trim.ch3 = 0;
            settings.trim.ch4 = 0;

            if (!sd_save_trims())
            {
              // handle failure
            }
            else
            {
              Nex::setValue(TRIMS_CH1, settings.trim.ch1);
              Nex::setValue(TRIMS_CH2, settings.trim.ch2);
              Nex::setValue(TRIMS_CH3, settings.trim.ch3);
              Nex::setValue(TRIMS_CH4, settings.trim.ch4);
            }
            break;
          }
          break;

        case UI::Buttons::P_trim::ch1_trim_plus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.trim.ch1 += TRIM_INCR;
            if (settings.trim.ch1 > 512)
              settings.trim.ch1 = 512;

            if (sd_save_trims())
              Nex::setValue(TRIMS_CH1, settings.trim.ch1);
            break;
          }
          break;

        case UI::Buttons::P_trim::ch1_trim_minus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.trim.ch1 -= TRIM_INCR;
            if (settings.trim.ch1 < -512)
              settings.trim.ch1 = -512;

            if (sd_save_trims())
              Nex::setValue(TRIMS_CH1, settings.trim.ch1);
            break;
          }
          break;

        case UI::Buttons::P_trim::ch2_trim_plus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.trim.ch2 += TRIM_INCR;
            if (settings.trim.ch2 > 512)
              settings.trim.ch2 = 512;

            if (sd_save_trims())
              Nex::setValue(TRIMS_CH2, settings.trim.ch2);
            break;
          }
          break;

        case UI::Buttons::P_trim::ch2_trim_minus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.trim.ch2 -= TRIM_INCR;
            if (settings.trim.ch2 < -512)
              settings.trim.ch2 = -512;

            if (sd_save_trims())
              Nex::setValue(TRIMS_CH2, settings.trim.ch2);
            break;
          }
          break;

        case UI::Buttons::P_trim::ch3_trim_plus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.trim.ch3 += TRIM_INCR;
            if (settings.trim.ch3 > 512)
              settings.trim.ch3 = 512;

            if (sd_save_trims())
              Nex::setValue(TRIMS_CH3, settings.trim.ch3);
            break;
          }
          break;

        case UI::Buttons::P_trim::ch3_trim_minus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.trim.ch3 -= TRIM_INCR;
            if (settings.trim.ch3 < -512)
              settings.trim.ch3 = -512;

            if (sd_save_trims())
              Nex::setValue(TRIMS_CH3, settings.trim.ch3);
            break;
          }
          break;

        case UI::Buttons::P_trim::ch4_trim_plus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.trim.ch4 += TRIM_INCR;
            if (settings.trim.ch4 > 512)
              settings.trim.ch4 = 512;

            if (sd_save_trims())
              Nex::setValue(TRIMS_CH4, settings.trim.ch4);
            break;
          }
          break;

        case UI::Buttons::P_trim::ch4_trim_minus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.trim.ch4 -= TRIM_INCR;
            if (settings.trim.ch4 < -512)
              settings.trim.ch4 = -512;

            if (sd_save_trims())
              Nex::setValue(TRIMS_CH4, settings.trim.ch4);
            break;
          }
          break;
        }
        break;
      }
      break;

    case Pages::expo:
      switch (type)
      {
      case UI::Type::page:
        switch (command)
        {
        case UI::Pages::Action::pre_init:
          Nex::setValue(EXPO_MODE_BTN, settings.expo.expo_mode);
          Nex::setValue(EXPO_CH1, settings.expo.ch1 * 10);
          Nex::setValue(EXPO_CH2, settings.expo.ch2 * 10);
          Nex::setValue(EXPO_CH3, settings.expo.ch3 * 10);
          Nex::setValue(EXPO_CH4, settings.expo.ch4 * 10);

          nexdata.page = Pages::expo;
          break;
        }
        break;

      case UI::Type::dualstatebutton:
        switch (index)
        {
        case UI::DualStateButton::P_expo::expo_mode:
          switch (command)
          {
          case UI::DualStateButton::Action::pop:
            settings.expo.expo_mode = !settings.expo.expo_mode;
            if (!sd_save_expos())
            {
              // handle failure
            }
          }
          break;
        }
        break;

      case UI::Type::button:
        switch (index)
        {
        case UI::Buttons::P_expo::reset_expos:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.expo.ch1 = -2.0F;
            settings.expo.ch2 = -2.0F;
            settings.expo.ch3 = -2.0F;
            settings.expo.ch4 = -2.0F;

            settings.expo.expo_mode = false;

            Nex::setValue(EXPO_MODE_BTN, settings.expo.expo_mode);
            Nex::setValue(EXPO_CH1, settings.expo.ch1 * 10);
            Nex::setValue(EXPO_CH2, settings.expo.ch2 * 10);
            Nex::setValue(EXPO_CH3, settings.expo.ch3 * 10);
            Nex::setValue(EXPO_CH4, settings.expo.ch4 * 10);
            if (!sd_save_expos())
            {
              // handle failure
            }
            break;
          }
          break;

        case UI::Buttons::P_expo::ch1_expo_plus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.expo.ch1 += EXPO_INCR;
            if (settings.expo.ch1 > -1.0F)
              settings.expo.ch1 = -1.0F;

            if (sd_save_expos())
              Nex::setValue(EXPO_CH1, settings.expo.ch1 * 10);
            break;
          }
          break;

        case UI::Buttons::P_expo::ch1_expo_minus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.expo.ch1 -= EXPO_INCR;
            if (settings.expo.ch1 < -4.0F)
              settings.expo.ch1 = -4.0F;

            if (sd_save_expos())
              Nex::setValue(EXPO_CH1, settings.expo.ch1 * 10);
            break;
          }
          break;

        case UI::Buttons::P_expo::ch2_expo_plus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.expo.ch2 += EXPO_INCR;
            if (settings.expo.ch2 > -1.0F)
              settings.expo.ch2 = -1.0F;

            if (sd_save_expos())
              Nex::setValue(EXPO_CH2, settings.expo.ch2 * 10);
            break;
          }
          break;

        case UI::Buttons::P_expo::ch2_expo_minus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.expo.ch2 -= EXPO_INCR;
            if (settings.expo.ch2 < -4.0F)
              settings.expo.ch2 = -4.0F;

            if (sd_save_expos())
              Nex::setValue(EXPO_CH2, settings.expo.ch2 * 10);
            break;
          }
          break;

        case UI::Buttons::P_expo::ch3_expo_plus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.expo.ch3 += EXPO_INCR;
            if (settings.expo.ch3 > -1.0F)
              settings.expo.ch3 = -1.0F;

            if (sd_save_expos())
              Nex::setValue(EXPO_CH3, settings.expo.ch3 * 10);
            break;
          }
          break;

        case UI::Buttons::P_expo::ch3_expo_minus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.expo.ch3 -= EXPO_INCR;
            if (settings.expo.ch3 < -4.0F)
              settings.expo.ch3 = -4.0F;

            if (sd_save_expos())
              Nex::setValue(EXPO_CH3, settings.expo.ch3 * 10);
            break;
          }
          break;

        case UI::Buttons::P_expo::ch4_expo_plus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.expo.ch4 += EXPO_INCR;
            if (settings.expo.ch4 > -1.0F)
              settings.expo.ch4 = -1.0F;

            if (sd_save_expos())
              Nex::setValue(EXPO_CH4, settings.expo.ch4 * 10);
            break;
          }
          break;

        case UI::Buttons::P_expo::ch4_expo_minus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.expo.ch4 -= EXPO_INCR;
            if (settings.expo.ch4 < -4.0F)
              settings.expo.ch4 = -4.0F;

            if (sd_save_expos())
              Nex::setValue(EXPO_CH4, settings.expo.ch4 * 10);
            break;
          }
          break;
        }
        break;
      }
      break;

    case Pages::dualrates:
      switch (type)
      {
      case UI::Type::page:
        switch (command)
        {
        case UI::Pages::Action::pre_init:
          Nex::setValue(DUAL_MODE_BTN, settings.dualrates.dual_mode);
          Nex::setValue(DUAL_CH1, settings.dualrates.ch1);
          Nex::setValue(DUAL_CH2, settings.dualrates.ch2);
          Nex::setValue(DUAL_CH3, settings.dualrates.ch3);
          Nex::setValue(DUAL_CH4, settings.dualrates.ch4);

          nexdata.page = Pages::dualrates;
          break;
        }
        break;

      case UI::Type::dualstatebutton:
        switch (index)
        {
        case UI::DualStateButton::P_dualrates::dual_mode:
          switch (command)
          {
          case UI::DualStateButton::Action::pop:
            settings.dualrates.dual_mode = !settings.dualrates.dual_mode;
            if (!sd_save_duals())
            {
              // handle failure
            }
          }
          break;
        }
        break;

      case UI::Type::button:
        switch (index)
        {
        case UI::Buttons::P_dualrates::reset_duals:
          settings.dualrates.ch1 = 100;
          settings.dualrates.ch2 = 100;
          settings.dualrates.ch3 = 100;
          settings.dualrates.ch4 = 100;
          settings.dualrates.dual_mode = false;

          if (sd_save_duals())
          {
            Nex::setValue(DUAL_MODE_BTN, settings.dualrates.dual_mode);
            Nex::setValue(DUAL_CH1, settings.dualrates.ch1);
            Nex::setValue(DUAL_CH2, settings.dualrates.ch2);
            Nex::setValue(DUAL_CH3, settings.dualrates.ch3);
            Nex::setValue(DUAL_CH4, settings.dualrates.ch4);
          }
          else
          {
            // handle failure
          }
          break;
        case UI::Buttons::P_dualrates::ch1_dual_plus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.dualrates.ch1 += DUAL_INCR;
            //                    if (settings.dualrates.ch1 > 100)
            //                      settings.dualrates.ch1 = 100;

            if (sd_save_duals())
              Nex::setValue(DUAL_CH1, settings.dualrates.ch1);
            break;
          }
          break;

        case UI::Buttons::P_dualrates::ch1_dual_minus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.dualrates.ch1 -= DUAL_INCR;
            if (settings.dualrates.ch1 < 0)
              settings.dualrates.ch1 = 0;

            if (sd_save_duals())
              Nex::setValue(DUAL_CH1, settings.dualrates.ch1);
            break;
          }
          break;

        case UI::Buttons::P_dualrates::ch2_dual_plus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.dualrates.ch2 += DUAL_INCR;
            //                    if (settings.dualrates.ch2 > 100)
            //                      settings.dualrates.ch2 = 100;

            if (sd_save_duals())
              Nex::setValue(DUAL_CH2, settings.dualrates.ch2);
            break;
          }
          break;

        case UI::Buttons::P_dualrates::ch2_dual_minus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.dualrates.ch2 -= DUAL_INCR;
            if (settings.dualrates.ch2 < 0)
              settings.dualrates.ch2 = 0;

            if (sd_save_duals())
              Nex::setValue(DUAL_CH2, settings.dualrates.ch2);
            break;
          }
          break;

        case UI::Buttons::P_dualrates::ch3_dual_plus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.dualrates.ch3 += DUAL_INCR;
            //                    if (settings.dualrates.ch3 > 100)
            //                      settings.dualrates.ch3 = 100;

            if (sd_save_duals())
              Nex::setValue(DUAL_CH3, settings.dualrates.ch3);
            break;
          }
          break;

        case UI::Buttons::P_dualrates::ch3_dual_minus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.dualrates.ch3 -= DUAL_INCR;
            if (settings.dualrates.ch3 < 0)
              settings.dualrates.ch3 = 0;

            if (sd_save_duals())
              Nex::setValue(DUAL_CH3, settings.dualrates.ch3);
            break;
          }
          break;

        case UI::Buttons::P_dualrates::ch4_dual_plus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.dualrates.ch4 += DUAL_INCR;
            //                    if (settings.dualrates.ch4 > 100)
            //                      settings.dualrates.ch4 = 100;

            if (sd_save_duals())
              Nex::setValue(DUAL_CH4, settings.dualrates.ch4);
            break;
          }
          break;

        case UI::Buttons::P_dualrates::ch4_dual_minus:
          switch (command)
          {
          case UI::Buttons::Action::pop:
            settings.dualrates.ch4 -= DUAL_INCR;
            if (settings.dualrates.ch4 < 0)
              settings.dualrates.ch4 = 0;

            if (sd_save_duals())
              Nex::setValue(DUAL_CH4, settings.dualrates.ch4);
            break;
          }
          break;
        }
        break;
      }
      break;

    case Pages::joysticks:
      switch (type)
      {
      case UI::Type::page:
        switch (command)
        {
        case UI::Pages::Action::pre_init:
          nexdata.page = Pages::joysticks;
          break;
        }
        break;
      }
      break;

    case Pages::inverse:
      switch (type)
      {
      case UI::Type::page:
        switch (command)
        {
        case UI::Pages::Action::pre_init:
          Nex::setValue(INVERSE_CH1_BTN, settings.inverse.ch1);
          Nex::setValue(INVERSE_CH2_BTN, settings.inverse.ch2);
          Nex::setValue(INVERSE_CH3_BTN, settings.inverse.ch3);
          Nex::setValue(INVERSE_CH4_BTN, settings.inverse.ch4);

          nexdata.page = Pages::inverse;
          break;
        }
        break;

      case UI::Type::dualstatebutton:
        switch (index)
        {
        case UI::DualStateButton::P_inverse::inv_ch1:
          switch (command)
          {
          case UI::DualStateButton::Action::pop:
            settings.inverse.ch1 = !settings.inverse.ch1;
            if (!sd_save_inverse())
            {
              // handle failure
            }
            break;
          }
          break;

        case UI::DualStateButton::P_inverse::inv_ch2:
          switch (command)
          {
          case UI::DualStateButton::Action::pop:
            settings.inverse.ch2 = !settings.inverse.ch2;
            if (!sd_save_inverse())
            {
              // handle failure
            }
            break;
          }
          break;

        case UI::DualStateButton::P_inverse::inv_ch3:
          switch (command)
          {
          case UI::DualStateButton::Action::pop:
            settings.inverse.ch3 = !settings.inverse.ch3;
            if (!sd_save_inverse())
            {
              // handle failure
            }
            break;
          }
          break;

        case UI::DualStateButton::P_inverse::inv_ch4:
          switch (command)
          {
          case UI::DualStateButton::Action::pop:
            settings.inverse.ch4 = !settings.inverse.ch4;
            if (!sd_save_inverse())
            {
              // handle failure
            }
            break;
          }
          break;
        }
      }
      break;
    }
  }
}

void Nex::terminate_packet()
{
  NEX_SERIAL.write(0xFF);
  NEX_SERIAL.write(0xFF);
  NEX_SERIAL.write(0xFF);
}

void Nex::setValue(char const *name, long number)
{
  NEX_SERIAL.print(name);
  NEX_SERIAL.print(".val=");
  NEX_SERIAL.print(number);

  Nex::terminate_packet();
}

void Nex::setColorPco(char const *name, unsigned color)
{
  NEX_SERIAL.print(name);
  NEX_SERIAL.print(".pco=");
  NEX_SERIAL.print(color);

  Nex::terminate_packet();
}

void Nex::setColorBco(char const *name, unsigned color)
{
  NEX_SERIAL.print(name);
  NEX_SERIAL.print(".bco=");
  NEX_SERIAL.print(color);

  Nex::terminate_packet();
}

void Nex::setText(char const *name, char const *text)
{
  NEX_SERIAL.print(name);
  NEX_SERIAL.print(".txt=\"");
  NEX_SERIAL.print(text);
  NEX_SERIAL.print("\"");

  Nex::terminate_packet();
}

void Nex::setEnable(char const *name, bool enable)
{
  NEX_SERIAL.print(name);
  NEX_SERIAL.print(".en=");
  NEX_SERIAL.print(enable);

  Nex::terminate_packet();
}

void Nex::changePage(byte page)
{
  NEX_SERIAL.print("page ");
  NEX_SERIAL.print(page);

  Nex::terminate_packet();
}
