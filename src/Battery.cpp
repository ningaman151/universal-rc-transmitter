#include <Battery.h>
#include <Time.h>
#include <Pins.h>
#include "Nextion.h"
#include <Helper.h>

namespace
{
  CircularBuffer<unsigned, BUFF_SIZE> bat_lvl_buff;
}

void battery_setup()
{
  //Unsure if this is the best approach
   for (int i = 0; i < BUFF_SIZE; ++i)
     bat_lvl_buff.push(100);
}

void battery_level(const char* num_name)
{
  static time_ms prev_time = millis();

  //Fill the buffer with values so the project doesn't auto shutdown'

  if (wait(prev_time, BUFF_INTERVAL))
  {
    float Vout = (float(analogRead(BATTERY_PIN)) * BATTERY_MAX_VOLTAGE) / 1024.0F;
    //float Vin = Vout * 2.6F;
    //Display battery voltage

    // Use linear equation faster than mapfloat
    unsigned bat_level_sample = mapfloat(Vout, 3.0F, 4.2F, 0.0F, 100.0F);
    //unsigned bat_level_sample = 83.334F * Vout - 250.0F;

    //unsigned bat_level_sample = Vout * 10.0F;

    //Nex::setValue(num_name, bat_level_sample);

    bat_lvl_buff.push(bat_level_sample);

    float bat_lvl_average = 0.0F;

    using index_t = decltype(bat_lvl_buff)::index_t;
    for (index_t i = 0; i < bat_lvl_buff.size(); ++i)
    {
      bat_lvl_average += float(bat_lvl_buff[i]) / float(bat_lvl_buff.size());
    }

    unsigned bat_lvl_av_int = unsigned(bat_lvl_average);

    //send data to nextion
    Nex::setValue(num_name, bat_lvl_av_int);
    
    //Nex::setValue(bar_name, bat_lvl_av_int);

    //    if(bat_lvl_av_int < 33)
    //    {
    //      Nex::setColorPco(num_name, RED);
    //      Nex::setColorPco(bar_name, RED);
    //
    ////    Nex::setEnable(BAT_BLINK_TIMER, true);
    //    }
    //    else if(bat_lvl_av_int < 66)
    //    {
    //      Nex::setColorPco(num_name, YELLOW);
    //      Nex::setColorPco(bar_name, YELLOW);
    //
    ////    Nex::setColorBco(num_name, 65535);
    ////    Nex::setEnable(BAT_BLINK_TIMER, false);
    //    }
    //    else
    //    {
    //      Nex::setColorPco(num_name, GREEN);
    //      Nex::setColorPco(bar_name, GREEN);
    //
    //      //Nex::setColorBco(BATTERY_NUM, 65535);
    //      //Nex::setEnable(BAT_BLINK_TIMER, false);
    //    }
    //Serial.println(bat_lvl_av_int);
  }
}
